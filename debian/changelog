pageedit (2.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.0+dfsg.
    https://sigil-ebook.com/blog/pageedit-2.0.2-released/
    https://sigil-ebook.com/blog/pageedit-2.1.0-released/
    https://sigil-ebook.com/blog/pageedit-2.2.0-released/
  * Update copyright.
  * Build with Qt6.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 28 Jul 2024 10:50:16 +0900

pageedit (2.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.0.0+dfsg.
    + https://sigil-ebook.com/blog/pageedit-2.0.0-released/
    + https://github.com/Sigil-Ebook/PageEdit/releases/tag/2.0.0
  * Refresh upstream gpg key that was expired.
  * Update watchfile to check the tags instead of releases in github.
  * Update symlink to the (removed) embedded jquery.
  * Update copyright.
  * Bump Standards-Version to 4.6.2, no changes needed.
  * Override lintian's package-does-not-install-examples.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 04 Sep 2023 14:03:14 +0530

pageedit (1.9.20+dfsg-1) unstable; urgency=medium

  * New upstream version 1.9.20+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.9.20
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 07 Sep 2022 20:14:15 +0200

pageedit (1.9.10+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Mattia Rizzolo ]
  * New upstream version 1.9.10+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.9.10
  * Update copyright.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 23 May 2022 17:13:37 +0200

pageedit (1.7.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable.
  * d/control: Bump Standards-Version to 4.6.0, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 21 Aug 2021 17:42:26 +0200

pageedit (1.7.0+dfsg-1) experimental; urgency=medium

  * New upstream version 1.7.0+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.7.0

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 28 Jul 2021 14:50:28 +0200

pageedit (1.6.0+dfsg-1) experimental; urgency=medium

  * New upstream version 1.6.0+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.6.0

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 30 May 2021 18:58:45 +0200

pageedit (1.5.0+dfsg-1) experimental; urgency=medium

  * New upstream version 1.5.0+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.5.0
  * Update copyright.
  * Fixup watchfile match for the changed github URLs.
  * Bump Standards-Version to 4.5.1, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 17 Apr 2021 20:29:07 +0200

pageedit (1.4.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.4.0+dfsg.
    https://github.com/Sigil-Ebook/PageEdit/releases/tag/1.4.0
  * Upstream removed the non-free js file, drop related patch.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 14 Nov 2020 16:09:41 +0100

pageedit (1.3.0+dfsg1-1) unstable; urgency=medium

  [ Baptiste Beauplat ]
  * Add missing copyright info for gumbo_subtree/*.
  * Add prorietary jquery.wrapSelection.js file to Files-Excluded.

  [ Mattia Rizzolo ]
  * New upstream version 1.3.0+dfsg1, removing the above jquery.wrapSelection.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 11 Nov 2020 17:24:05 +0100

pageedit (1.3.0+dfsg-1) unstable; urgency=medium

  * Initial release.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 11 Aug 2020 09:16:32 +0200
